"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const bookRouter_1 = require("./routes/bookRouter");
const collectionsrouter_1 = require("./routes/collectionsrouter");
const cartRouter_1 = require("./routes/cartRouter");
const middleware_1 = require("./middleware");
const app = express_1.default();
const port = 8000 || process.env.PORT;
app.use(express_1.default.json());
app.all('*', middleware_1.addResponseHeaders);
app.use("/books", bookRouter_1.booksRouter);
app.use("/collections", collectionsrouter_1.collectionsRouter);
app.use("/cart", cartRouter_1.cartRouter);
// export { app };
app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`server started at http://localhost:${port}`);
});
//# sourceMappingURL=app.js.map