"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addResponseHeaders = void 0;
exports.addResponseHeaders = (req, res, next) => {
    // const origin: any = req.headers.origin;
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    // if (req.method === 'OPTIONS') {
    //     return res.sendStatus(200);
    // } else {
    next();
    // }
};
//# sourceMappingURL=middleware.js.map