"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cartRouter = void 0;
const express_1 = __importDefault(require("express"));
const cartService_1 = require("../services/cartService");
exports.cartRouter = express_1.default.Router();
exports.cartRouter.get("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const items = yield cartService_1.cartService.getCart();
        // console.log("cart /get",items)
        res.status(200).send(items);
    }
    catch (e) {
        res.status(404).send(e.message);
    }
}));
exports.cartRouter.post("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const items = yield cartService_1.cartService.addBookToCart(req);
        res.status(200).send(items);
    }
    catch (e) {
        res.status(404).send(e.message);
    }
}));
exports.cartRouter.put("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const items = yield cartService_1.cartService.updateCartItem(req);
        res.status(200).send(items);
    }
    catch (e) {
        res.status(404).send(e.message);
    }
}));
exports.cartRouter.delete("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const items = yield cartService_1.cartService.clearCart(req);
        res.status(200).send(items);
    }
    catch (e) {
        res.status(404).send(e.message);
    }
}));
//# sourceMappingURL=cartRouter.js.map