"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.collectionsRouter = void 0;
const express_1 = __importDefault(require("express"));
const collectionService_1 = require("../services/collectionService");
exports.collectionsRouter = express_1.default.Router();
exports.collectionsRouter.get("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("collectionsRouter:get")
    try {
        const items = yield collectionService_1.collectionsService.getCollection();
        // console.log("items", items)
        res.status(200).send(items);
    }
    catch (e) {
        res.status(404).send(e.message);
    }
}));
exports.collectionsRouter.post("/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    // console.log("collectionsRouter:post")
    try {
        const items = yield collectionService_1.collectionsService.addBookToCollection(req);
        // console.log("collectionsRouter:post:resp",items)
        res.status(200).send(items);
    }
    catch (e) {
        // console.log("collectionsRouter:post:error",e)
        res.status(404).send(e.message);
    }
}));
//# sourceMappingURL=collectionsrouter.js.map