"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.booksCollections = void 0;
// const Request = require('request-promise');//
// var Request = require('request');
// import { Request, Response } from 'express';
// import {got} from 'got'
// const got = require('got');
const got = __importStar(require("got"));
// var request = require('request-promise');
const booksUrl = 'https://www.googleapis.com/books/v1/volumes';
class BooksCollections {
    constructor() {
        this.invoke = (req) => __awaiter(this, void 0, void 0, function* () {
            const searchString = req.query.q;
            try {
                return yield got.get(`${booksUrl}/?q=${searchString}`, { json: true });
            }
            catch (error) {
                return error;
            }
        });
        this.getBook = (req) => __awaiter(this, void 0, void 0, function* () {
            const id = req.params.id;
            try {
                return yield got.get(`${booksUrl}/${id}`, { json: true });
                // const res = await Request(`${books_url}/${id}`);
                // return h.response(res).code(200);
            }
            catch (error) {
                return error;
            }
        });
    }
}
exports.booksCollections = new BooksCollections();
//# sourceMappingURL=booksCollection.js.map