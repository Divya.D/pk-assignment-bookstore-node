"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cartService = void 0;
let cart = [];
class CartService {
    constructor() {
        this.getCart = () => {
            // console.log("cart:get",cart)
            return cart;
        };
        this.addBookToCart = (req, h) => {
            const cartItem = req.body;
            cart.push(cartItem);
            // console.log("cart",cart)
            return { res: 'success' };
        };
        this.updateCartItem = (req, h) => {
            cart = cart.map((item) => (item.id === req.payload.id) ?
                (Object.assign(Object.assign({}, item), req.payload.changes)) : item);
            return { result: "cart Updated successfully" };
        };
        this.clearCart = (req, h) => {
            if (req.query.id) {
                cart = cart.filter((item) => item.book.id !== req.query.id);
            }
            else {
                cart = [];
            }
            return { result: "Item(s) cleared" };
        };
    }
}
exports.cartService = new CartService();
//# sourceMappingURL=cartService.js.map