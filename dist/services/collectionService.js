"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.collectionsService = void 0;
let collection = [];
class CollectionsService {
    constructor() {
        this.getCollection = () => {
            // let collection: any = [];
            return collection;
        };
        this.addBookToCollection = (req) => {
            // console.log(req.body)
            collection = [...collection, ...req.body];
            return { result: "success" };
        };
    }
}
exports.collectionsService = new CollectionsService();
//# sourceMappingURL=collectionService.js.map