import express from "express";
import { booksRouter } from './routes/bookRouter';
import { collectionsRouter } from './routes/collectionsrouter';
import { cartRouter } from './routes/cartRouter';
import { addResponseHeaders } from './middleware';


const app: express.Application = express();
const port = 8000 || process.env.PORT;

app.use(express.json());
app.all('*', addResponseHeaders);

app.use("/books", booksRouter);
app.use("/collections", collectionsRouter);
app.use("/cart", cartRouter);

app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});