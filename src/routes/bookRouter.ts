import express, { Request, Response } from "express";
import { booksCollections } from '../services/booksCollection';

export const booksRouter = express.Router();

booksRouter.get("/", async (req: Request, res: Response) => {
    try {
        const items: any = await booksCollections.invoke(req, res);
        console.log("items.body", items)
        res.status(200).send(items.body ? items.body : items);
    } catch (e) {
        console.log("items.e", e)

        res.status(404).send(e.message);
    }
});
booksRouter.get('/:id', async (req: Request, res: Response) => {
    try {
        const items: any = await booksCollections.getBook(req, res);

        res.status(200).send(items.body);
    } catch (e) {
        res.status(404).send(e.message);
    }
});
