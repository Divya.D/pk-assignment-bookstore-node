import express, { Request, Response } from "express";
import { cartService } from '../services/cartService';

export const cartRouter = express.Router();

cartRouter.get("/", async (req: Request, res: Response) => {
    try {
        const items: any = await cartService.getCart();
        res.status(200).send(items);
    } catch (e) {
        res.status(404).send(e.message);
    }
});
cartRouter.post("/", async (req: Request, res: Response) => {
    try {
        const items: any = await cartService.addBookToCart(req);

        res.status(200).send(items);
    } catch (e) {
        res.status(404).send(e.message);
    }
});
cartRouter.put("/", async (req: Request, res: Response) => {
    console.log("put")
    try {
        console.log("put:update")
        const items: any = await cartService.updateCartItem(req);
        console.log("put:items",items)
        res.status(200).send(items);
    } catch (e) {
        console.log("put:e",e)

        res.status(404).send(e.message);
    }
});
cartRouter.delete("/", async (req: Request, res: Response) => {
    try {
        const items: any = await cartService.clearCart(req)

        res.status(200).send(items);
    } catch (e) {
        res.status(404).send(e.message);
    }
});
