import express, { Request, Response } from "express";
import { collectionsService } from '../services/collectionService';

export const collectionsRouter = express.Router();

collectionsRouter.get("/", async (req: Request, res: Response) => {

    try {
        const items: any = await collectionsService.getCollection();
        res.status(200).send(items);
    } catch (e) {
        res.status(404).send(e.message);
    }
});
collectionsRouter.post("/", async (req: Request, res: Response) => {
    try {
        const items: any = await collectionsService.addBookToCollection(req);

        res.status(200).send(items);
    } catch (e) {
        res.status(404).send(e.message);
    }
});
