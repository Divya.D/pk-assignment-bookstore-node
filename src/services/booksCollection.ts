
import * as got from 'got';

const booksUrl = 'https://www.googleapis.com/books/v1/volumes';


export interface IdInterface {
    query: { q: string };
}
class BooksCollections {
    public invoke: any = async (req: IdInterface) => {
        const searchString = req.query.q;
        try {
            return await got.get(`${booksUrl}/?q=${searchString}`, { json: true });
        } catch (error) {
            return error;
        }
    };

    public getBook: any = async (req: any) => {
        const id = req.params.id;
        try {
            return await got.get(`${booksUrl}/${id}`, { json: true });
          
        } catch (error) {
            return error;
        }
    }
}
export const booksCollections: any = new BooksCollections();