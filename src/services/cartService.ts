
// const Request = require('request-promise');//
// var Request = require('request');
import { Request, Response } from 'express';

let cart: any = [];




class CartService {
    public getCart = () => {
        return cart;
    }
    public addBookToCart = (req: any, h: Response) => {
        const cartItem = req.body;
        cart.push(cartItem);
        return { res: 'success' };
    }
    public updateCartItem = (req: any, h: Response) => {
        cart = cart.map((item: any) => (item.id === req.body.id) ?
            ({ ...item, ...req.body.changes }) : item)
        return { result: "cart Updated successfully" };
    }
    public clearCart = (req: any, h: Response) => {
        if (req.query.id) {
            cart = cart.filter((item: any) => item.book.id !== req.query.id)
        } else {
            cart = [];
        }
        return { result: "Item(s) cleared" }
    }
}
export const cartService: any = new CartService();