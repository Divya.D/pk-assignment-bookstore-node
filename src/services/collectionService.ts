
let collection: any = [];



class CollectionsService {
    public getCollection = () => {
        return collection;
    }
    public addBookToCollection = (req: any) => {
        collection = [...collection, ...req.body]
        return { result: "success" };
    }
}
export const collectionsService: any = new CollectionsService();